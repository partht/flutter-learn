// Import libraries
import 'package:flutter/material.dart';
import 'package:http/http.dart' show get;
import './models/image_modal.dart';
import 'dart:convert';
import './widgets/image_list.dart';

// Create a class that wll be our custom widget
// This class must extend StatelessWidget base class

class App extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return AppState();
  }
}

class AppState extends State<App> {
  int counter = 1;
  List<ImageModel> images = [];

  void fetchImage() async {
    var url = Uri.parse('https://jsonplaceholder.typicode.com/photos/$counter');
    var result = await get(url);
    var imageModel = ImageModel.fromJson(json.decode(result.body));
    setState(() {
      images.add(imageModel);
      counter += 1;
    });
  }

// Must define build method that returns the widget that this method will show
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: ImageList(images),
        appBar: AppBar(
          title: Text('Lets see some images'),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: fetchImage,
          child: const Icon(Icons.add),
          backgroundColor: Colors.green,
        ),
      ),
    );
    //throw UnimplementedError();
  }
}
